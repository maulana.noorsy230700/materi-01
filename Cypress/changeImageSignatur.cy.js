/// <reference types="cypress" />
import 'cypress-file-upload';

describe('Change Image Signature Automation Test', () => {
    it('Memastikan Fitur Change Image Signature Berjalan Dengan Baik', () => {
        cy.on('uncaught:exception', (err, runnable) => {
            return false;
        });
        cy.visit('https://app.privy.id');
        cy.wait(1000);
        cy.get('[name="user[privyId]"]').type('M9649235', { force: true });
        cy.wait(1000);
        cy.contains('CONTINUE').click({ force: true });
        cy.wait(1000);
        cy.get('[name="user[secret]"]').type('Pacitan230700', { force: true });
        cy.wait(1000);
        cy.contains('CONTINUE').click();
        cy.wait(1000);
        cy.contains('Change My Signature Image').click();
        cy.wait(1000);
        cy.get('select[class="mb-3 custom-select"]').select('true', {
            force: true,
        });
        cy.wait(1000);
        cy.get(
            'button[class="btn btn-danger d-inline-block d-lg-none icon-only"]'
        ).click({ force: true });
        cy.wait(1000);
        cy.get('select[class="w-50 custom-select"]').select('Satisfy', {
            force: true,
        });
        cy.wait(1000);
        cy.contains('Black').click();
        cy.wait(1000);
        cy.get('input[id="name-signature"]')
            .clear()
            .type('Maulana hn', { force: true });
        cy.wait(1000);
        cy.get('input[id="name-initial"]').clear().type('Maulana', { force: true });
        cy.wait(1000);
        cy.contains('Save').click();
        cy.wait(1000);
        cy.contains('Set as default').click();
        cy.wait(1000);
        cy.get(
            'button[class="btn btn-danger d-inline-block d-lg-none icon-only"]'
        ).click({ force: true });
        cy.wait(1000);
        cy.get('a[href="/settings/signatures/image"]').click();
        cy.wait(1000);
        cy.get(
            'div[class="cursor-pointer text-center dashed-line"] div[class="d-flex cursor-pointer text-center text-primary box-height"]'
        ).click();
        cy.wait(1000);
        cy.get('input[name="image"]').attachFile('sign.png');
        cy.wait(1000);
        cy.contains('Crop').click();
        cy.wait(1000);
        cy.contains('Apply').click();
        cy.wait(1000);
        cy.get(
            'div[class="cursor-pointer text-center w-50 dashed-line"] div[class="d-flex cursor-pointer text-center text-primary box-height"]'
        ).click();
        cy.wait(1000);
        cy.get('input[name="image"]').attachFile('sign.png');
        cy.wait(1000);
        cy.contains('Crop').click();
        cy.wait(1000);
        cy.contains('Apply').click();
        cy.wait(1000);
        cy.contains('Save').click();
        cy.wait(1000);
        cy.contains('Set as default').click();
    });
});