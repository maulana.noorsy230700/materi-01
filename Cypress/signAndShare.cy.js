/// <reference types="cypress" />
import 'cypress-file-upload';

describe('Sign and Share', () => {
    it('Sign and Share', () => {
        cy.visit('https://app.privy.id');
        cy.wait(1000);
        cy.get('[name="user[privyId]"]').type('M9649235', { force: true });
        cy.wait(1000);
        cy.contains('CONTINUE').click({ force: true });
        cy.wait(1000);
        cy.get('[name="user[secret]"]').type('Pacitan230700', { force: true });
        cy.wait(1000);
        cy.contains('CONTINUE').click();
        cy.wait(1000);
        cy.get('[id="v-step-0"]').click();
        cy.wait(1000);
        cy.contains('Sign & Request').click();
        cy.wait(1000);
        cy.get('[class="workflow-upload__droparea"]').attachFile({ filePath: 'tesUploadFile.pdf', allowEmpty: true }, {
            subjectType: 'drag-n-drop',
        });
        cy.wait(1000);
        cy.get('input[maxlength="200"]').clear().type('Dummy File');
        cy.wait(1000);
        cy.get('button[class="btn btn-danger"]').click();
        cy.wait(1000);
        cy.get('button[id="step-document-1"]').click();
        cy.wait(1000);
        cy.get('button[class="btn btn-danger"]').click();
        cy.wait(1000);
        cy.get('button[class="btn btn-success mx-2 my-2"]').click();
        cy.wait(1000);
        cy.get('input[type="radio"]').check('qrcode', { force: true });
        cy.wait(1000);
        cy.get('button[form="form-sign"]').click();
        cy.wait(1000);
        cy.get('input[type="radio"]', { timeout: 300000 }).check('serial', {
            force: true,
        });
        cy.wait(1000);
        cy.get('input[type="checkbox"]').check('true', { force: true });
        cy.wait(1000);
        cy.get('button[class="btn btn-light btn-block"]').click();
        cy.wait(1000);
        cy.get('textarea[id="document-desc"]').type('Test Sign Dummy File !');
        cy.wait(1000);
        cy.contains('Apply').click();
        cy.wait(1000);
        cy.get('button[id="v-recipient-1"]').click();
        cy.wait(1000);
        cy.contains('Enter PrivyID').type('W10134572');
        cy.wait(1000);
        cy.contains('(WHRASPATHI YATINORA JANNATA)').click();
        cy.wait(1000);
        cy.get('input[type="radio"]').check({ force: true });
        cy.wait(1000);
        cy.contains('- Select Role -').click();
        cy.wait(1000);
        cy.get('ul[class="multiselect__content"] input[type="checkbox"]')
            .first()
            .check('true', { force: true });
        cy.wait(1000);
        cy.get('button[class="btn btn-danger pr-4 buttons"]').click();
        cy.wait(1000);
        cy.contains('Continue').click();
        cy.wait(1000);
        cy.get('button[id="step-document-0"]').click();
        cy.wait(1000);
        cy.contains('WHRASPATHI YATINORA JANNATA').click();
        cy.wait(1000);
        cy.contains('Done').click();
        cy.wait(1000);
        cy.contains('Already Signed').should('exist');
    });
});